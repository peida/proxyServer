/*
 * Copyright (c) 2014.
 * 游戏服务器核心代码编写人陈磊拥有使用权
 * 联系方式：E-mail:13638363871@163.com ;qq:502959937
 * 个人博客主页：http://my.oschina.net/chenleijava
 */

package nettyProxyEngine4;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * @author 石头哥哥
 *         Date: 13-11-27</br>
 *         Time: 上午11:20</br>
 *         Package: com.dc.gameserver.baseConfig</br>
 *         注解：游戏服务器配置文件加载
 */
public class proxyServerConfig {


    public static class  DEFAULT_VALUE{
        /**
         * 文件配置路径
         */
        public static class FILE_PATH{
            public static  String PROXY_SERVER="res/proxyServer.properties";
            public static  String LOG4J="res/log4j.xml";
        }

        /**
         * 游戏基础配置
         */
        public static class GAME_VALUE{

        }

        /**
         * 游戏服务器底层配置
         */
        public static class SERVER_VALUE{

            //启用基于jni的native epoll
            public static boolean nativeEpoll;

           // #是否开启代理服务器
           public static boolean    proxyServer;
            //#代理服务器端口
            public static  int    proxyServerPort;
              //      #游戏服务器ip地址
           public static  String  remoteHost ;
           public static  int    remotePort;

        }

    }

    /**
     * 初始化代理服务器配置参数
     * @throws org.apache.commons.configuration.ConfigurationException
     */
    public static void IntiProxyConfig() throws ConfigurationException{
        PropertiesConfiguration proxy_config= new PropertiesConfiguration(DEFAULT_VALUE.FILE_PATH.PROXY_SERVER);
        DEFAULT_VALUE.SERVER_VALUE.proxyServer=proxy_config.getBoolean("proxyServer");
        DEFAULT_VALUE.SERVER_VALUE.proxyServerPort=proxy_config.getInt("proxyServerPort");
        DEFAULT_VALUE.SERVER_VALUE.remoteHost =proxy_config.getString("remoteHost");
        DEFAULT_VALUE.SERVER_VALUE.remotePort =proxy_config.getInt("remotePort");

    }


}
