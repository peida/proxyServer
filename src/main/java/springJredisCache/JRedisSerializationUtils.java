/*
 * Copyright (c) 2013.
 *  游戏服务器核心代码编写人陈磊拥有使用权
 *  联系方式：E-mail:13638363871@163.com ;qq:502959937
 *  个人博客主页：http://my.oschina.net/chenleijava
 */

package springJredisCache;


import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author 石头哥哥 </br>
 *         dcserver1.3 </br>
 *         Date:14-1-9 </br>
 *         Time:下午4:00 </br>
 *         Package:{@link springJredisCache}</br>
 *         Comment： 对象序列化工具类   基于kryo的序列化方案
 *         com.esotericsoftware.kryo.KryoException: Encountered unregistered class ID: 1017252323
 */
public class JRedisSerializationUtils {

    private JRedisSerializationUtils(){}

    private static final Kryo kryo = new Kryo();

    //基于kryo序列换方案
    /**
     * 将对象序列化为字节数组
     * @param obj
     * @return   字节数组
     * @throws JRedisCacheException
     */
    public static byte[] kryoSerialize(Object obj) throws JRedisCacheException {
        if (obj==null) throw new JRedisCacheException("obj can not be null");
        ByteArrayOutputStream byteArrayOutputStream=null;
        Output output =null;
        try {
            byteArrayOutputStream=new ByteArrayOutputStream();      //缓冲数据
            output = new Output(byteArrayOutputStream);
            kryo.writeClassAndObject(output, obj);
            return output.toBytes();
        }catch (JRedisCacheException e){
            throw new JRedisCacheException("Serialize obj exception");
        }finally {
            try {
                obj=null;
                if (byteArrayOutputStream!=null){
                    byteArrayOutputStream.close();
                    byteArrayOutputStream=null;
                }
                if (output!=null){
                    output.close();   /** Writes the buffered bytes to the underlying OutputStream, if any .flush();. */
                    output=null;
                }
            } catch (IOException ee) {
                ee.printStackTrace();
            }
        }
    }

    /**
     * 将字节数组反序列化为对象
     * @param bytes    字节数组
     * @return           object
     * @throws JRedisCacheException
     */
    public static Object kryoDeserialize(byte[] bytes) throws JRedisCacheException {
        Input input = null;
        if (bytes==null) throw new JRedisCacheException("bytes can not be null");
        try {
            input = new Input(bytes);
            return kryo.readClassAndObject(input);
        }catch (JRedisCacheException e){
            throw new JRedisCacheException("Deserialize bytes exception");
        }finally {
            bytes=null;
            if (input!=null){
                input.close();
                input=null;
            }
        }
    }

//    //基于FST 的序列化方案
//    /**
//     * <p>Serializes an <code>Object</code> to a byte array for
//     * storage/serialization.</p>
//     *
//     * @param obj  the object to serialize to bytes
//     * @return a byte[] with the converted Serializable
//     * @throws JRedisCacheException (runtime) if the serialization fails
//     */
//    public static byte[] fastSerialize(Object obj) {
//        ByteArrayOutputStream byteArrayOutputStream = null;
//        FSTObjectOutput out = null;
//        try {
//            // stream closed in the finally
//            byteArrayOutputStream = new ByteArrayOutputStream();
//            out = new FSTObjectOutput(byteArrayOutputStream);
//            out.writeObject(obj);
//            return byteArrayOutputStream.toByteArray();
//        } catch (IOException ex) {
//            throw new JRedisCacheException(ex);
//        } finally {
//            try {
//                obj=null;
//                if (out != null) {
//                    out.close();    //call flush byte buffer
//                    out=null;
//                }
//                if (byteArrayOutputStream!=null){
//                    byteArrayOutputStream.close();
//                    byteArrayOutputStream=null;
//                }
//            } catch (IOException ex) {
//                // ignore close exception
//            }
//        }
//    }
//
//
//    // Deserialize
//    //-----------------------------------------------------------------------
//    /**
//     * <p>Deserializes a single <code>Object</code> from an array of bytes.</p>
//     *
//     * @param objectData  the serialized object, must not be null
//     * @return the deserialized object
//     * @throws IllegalArgumentException if <code>objectData</code> is <code>null</code>
//     * @throws JRedisCacheException (runtime) if the serialization fails
//     */
//    public static Object fastDeserialize(byte[] objectData) {
//        ByteArrayInputStream byteArrayInputStream = null;
//        FSTObjectInput in = null;
//        try {
//            // stream closed in the finally
//            byteArrayInputStream = new ByteArrayInputStream(objectData);
//            in = new FSTObjectInput(byteArrayInputStream);
//            return in.readObject();
//        } catch (ClassNotFoundException ex) {
//            throw new JRedisCacheException(ex);
//        } catch (IOException ex) {
//            throw new JRedisCacheException(ex);
//        } finally {
//            try {
//                objectData=null;
//                if (in != null) {
//                    in.close();
//                    in=null;
//                }
//                if (byteArrayInputStream!=null){
//                    byteArrayInputStream.close();
//                    byteArrayInputStream=null;
//                }
//            } catch (IOException ex) {
//                // ignore close exception
//            }
//        }
//    }

}
