/*
 * Copyright (c) 2014.
 * 游戏服务器核心代码编写人陈磊拥有使用权
 * 联系方式：E-mail:13638363871@163.com ;qq:502959937
 * 个人博客主页：http://my.oschina.net/chenleijava
 */

package nettyProxyEngine4;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.DefaultEventExecutorGroup;

/**
 * @author 石头哥哥 </br>
 *         dcServer1.7 </br>
 *         Date:14-2-24 </br>
 *         Time:下午3:28 </br>
 *         Package:{@link nettyProxyEngine4}</br>
 *         Comment：
 */
public class ProxyServerChannelInitializer extends ChannelInitializer<SocketChannel> {


    private final String remoteHost;
    private final int remotePort;

    public ProxyServerChannelInitializer(String remoteHost, int remotePort) {
        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
    }


    /**
     * This method will be called once the {@link io.netty.channel.Channel} was registered. After the method returns this instance
     * will be removed from the {@link io.netty.channel.ChannelPipeline} of the {@link io.netty.channel.Channel}.
     *
     * @param ch the {@link io.netty.channel.Channel} which was registered.
     * @throws Exception is thrown if an error occurs. In that case the {@link io.netty.channel.Channel} will be closed.
     */
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ch.pipeline().addLast(new LoggingHandler(LogLevel.DEBUG))
        .addLast(new ProxyFrontendDecoder(20000,0,2 ,0,2))
        .addLast(new DefaultEventExecutorGroup(Runtime.getRuntime().availableProcessors(),
                new PriorityThreadFactory("#+逻辑处理线程+#", Thread.NORM_PRIORITY)), new ProxyFrontendServerHandler(remoteHost, remotePort));
    }
}
