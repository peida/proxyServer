/*
 * Copyright (c) 2014.
 * 游戏服务器核心代码编写人陈磊拥有使用权
 * 联系方式：E-mail:13638363871@163.com ;qq:502959937
 * 个人博客主页：http://my.oschina.net/chenleijava
 */

package nettyProxyEngine4;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author 石头哥哥 </br>
 *         dcServer1.7 </br>
 *         Date:14-2-24 </br>
 *         Time:下午5:54 </br>
 *         Package:{@link nettyProxyEngine4}</br>
 *         Comment：线程池工厂
 */
public class PriorityThreadFactory   implements ThreadFactory {

        private int _prio;
        private String _name;
        private AtomicInteger _threadNumber = new AtomicInteger(1);
        private ThreadGroup _group;

        /**
         *
         * @param name 线程池名
         * @param priority   线程池优先级
         */
        public PriorityThreadFactory(String name, int priority){
            _prio = priority;
            _name = name;
            _group = new ThreadGroup(_name);
        }

        @Override
        public Thread newThread(Runnable r){
            Thread t = new Thread(_group, r);
            t.setName(_name + "-"+"#-" + _threadNumber.getAndIncrement());
            t.setPriority(_prio);
            return t;
        }

    public ThreadGroup getGroup(){
        return _group;
    }
}
