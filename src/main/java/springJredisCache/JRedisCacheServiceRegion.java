/*
 * Copyright (c) 2013.
 *  游戏服务器核心代码编写人陈磊拥有使用权
 *  联系方式：E-mail:13638363871@163.com ;qq:502959937
 *  个人博客主页：http://my.oschina.net/chenleijava
 */

package springJredisCache;

/**
 * @author 石头哥哥 </br>
 *         dcserver1.3 </br>
 *         Date:14-1-10 </br>
 *         Time:下午6:56 </br>
 *         Package:{@link springJredisCache </br>
 *         Comment： redis区域前缀       标识内存块
 */
public class JRedisCacheServiceRegion {

    /**
     * for redis
     * 数据 region
     */
    public static class DATABASE_REGION{
        protected static String ROLE_ID_REGION="t_role";         //角色表   region
        protected static String EQU_ID_REGION="t_equ";           //装备表   region
        protected static String BOOK_ID_REGION="t_book";         //秘籍表   region
        protected static String ITEM_ID_REGION="t_props";        //道具表  region
        protected static String AREA_ID_REGION="t_area";           //area表   region
        protected static String LEVEL_ID_REGION="t_level"; //关卡表  region
        protected static String SPRITE_ID_REGION="t_sprite";              //灵脉 灵泉 蒲团
        protected static String BATTLE_HEART_ID_REGION="t_heartBattle";              //心魔战斗中记录数据       region
        protected static String BATTLE_HEART_RANK_ID_REGION="t_heartBattle_rank";              //心魔战斗排行       region

        //游戏缓存队列
        protected static String ROLE_LIST="role_list";
    }


    /**
     *   游戏服务器队列  存储在线人数会话
     * @return
     */
    public static String ROLE_LIST(){
        return DATABASE_REGION.ROLE_LIST ;
    }


    /**
     * 角色 心魔战斗排行
     * @param roleID 扮演角色编号
     * @return redis区域前缀
     */
    public static String BATTLE_HEART_RANK_ID_REGION(int roleID){
        return DATABASE_REGION.BATTLE_HEART_RANK_ID_REGION + roleID;
    }



    /**
     * 角色 心魔战斗中记录数据
     * @param roleID 扮演角色编号
     * @return redis区域前缀
     */
    public static String BATTLE_HEART_ID_REGION(int roleID){
        return DATABASE_REGION.BATTLE_HEART_ID_REGION + roleID;
    }


    /**
     * 角色
     * @param roleID 扮演角色编号
     * @return redis区域前缀
     */
    public static String SPRITE_REGION(int roleID){
        return DATABASE_REGION.SPRITE_ID_REGION + roleID;
    }

    /**
     * 角色
     * @param roleID 扮演角色编号
     * @return redis区域前缀
     */
    public static String ROLE_REGION(int roleID){
        return DATABASE_REGION.ROLE_ID_REGION + roleID;
    }

    /**
     * 装备
     * @param roleID   扮演角色编号
     * @return   redis区域前缀
     */
    public static String EQU_REGION(int roleID){
        return    DATABASE_REGION.EQU_ID_REGION+roleID;
    }

    /**
     * 秘籍
     * @param roleID    扮演角色编号
     * @return    redis区域前缀
     */
    public static String BOOK_REGION(int roleID){
        return    DATABASE_REGION.BOOK_ID_REGION+roleID;
    }

    /**
     *  道具
     * @param roleID    扮演角色编号
     * @return   redis区域前缀
     */
    public static String ITEM_REGION(int roleID){
        return    DATABASE_REGION.ITEM_ID_REGION+roleID;
    }

    /**
     * area
     * @param roleID    扮演角色编号
     * @return   redis区域前缀
     */
    public static String AREA_REGION(int roleID){
        return    DATABASE_REGION.AREA_ID_REGION+roleID;
    }

    /**
     * 关卡
     * @param roleID    扮演角色编号
     * @return    redis区域前缀
     */
    public static String LEVEL_REGION(int roleID){
        return    DATABASE_REGION.LEVEL_ID_REGION+roleID;
    }

}
